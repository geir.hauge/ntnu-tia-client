import json

import pytest

from ntnu_tia_client import models


@pytest.fixture
def bas_person_data(base_url):
    return json.loads('''
        {
          "person": {
            "bas_person": {
              "lastUpdated": 1583335818619,
              "kontaktinformasjon": {
                "sip_address": "ola.normann123@example.com",
                "email_work": "ola.normann123@example.com"
              },
              "gender": "m",
              "keys": {
                "ansattnr": "10203040",
                "feideid": "olanormann@example.com",
                "basid": "12345",
                "email": "ola.normann123@example.com",
                "username": "olanormann"
              },
              "roles": {
                "arbeidstaker": [
                  {
                    "qualifier": {
                      "position_category": "fast"
                    },
                    "from": 1482793200000,
                    "actsOn": {
                      "costcenter": "194167700"
                    }
                  }
                ],
                "category": [
                  {
                    "qualifier": {
                      "position_category": "tekadm"
                    },
                    "from": 1581634800000,
                    "actsOn": {
                      "costcenter": "194167700"
                    }
                  }
                ]
              },
              "name": {
                "displayname": "Ola \\"The Viking\\" Normann",
                "last_name": "Normann",
                "first_name": "Ola",
                "username": "oldanormann"
              },
              "dateOfBirth": -2208988800000,
              "status": {
                "accountActivated": "J",
                "reservePublish": "N"
              }
            }
          },
          "api": {
            "path": "/V4",
            "vendor": "NTNU - IT Avdelingen",
            "deprecated": false,
            "name": "NTNU:TIA:REST",
            "version": "V4.0"
          }
        }
    ''')


@pytest.fixture
def mock_api(endpoints, mock_api, bas_person_data):
    mock_api.get(endpoints.get_bas_person_url('username', 'olanormann'),
                 text=json.dumps(bas_person_data))
    return mock_api


def test_get_bas_person(endpoints, client, mock_api, bas_person_data):
    mock_api.get(
        endpoints.get_bas_person_url('username', 'olanormann'),
        text=json.dumps(bas_person_data))

    assert client.get_bas_person('username', 'olanormann') == models.Person.from_dict(
        bas_person_data["person"]["bas_person"])
