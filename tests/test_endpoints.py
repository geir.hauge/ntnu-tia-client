# import pytest

from ntnu_tia_client.client import quote_path_arg, urljoin


def test_quote():
    assert quote_path_arg('foo/bar') == 'foo%2Fbar'


def test_join():
    url = urljoin('https://localhost/foo', 'bar')
    assert url == 'https://localhost/foo/bar'


def test_join_multi():
    url = urljoin('https://localhost/foo', 'bar', 'baz')
    assert url == 'https://localhost/foo/bar/baz'


def test_base_url(endpoints, base_url):
    assert endpoints.baseurl == base_url
    url = endpoints.get_bas_person_url('username', 'olanormann')
    assert url.startswith(base_url)


def test_get_bas_person_url(endpoints, base_url):
    url = endpoints.get_bas_person_url('username', 'olanormann', query={
                                       'fields': 'person.bas_person'})
    print('got url', url)
    assert url == urljoin(
        base_url, 'person/username/olanormann/?fields=person.bas_person')


def test_get_bas_person_url_escape(endpoints, base_url):
    url = endpoints.get_bas_person_url('foo/bar', 'baz')
    print('got url', url)
    assert url == urljoin(
        base_url, 'person/foo%2Fbar/baz/?fields=person.bas_person')
