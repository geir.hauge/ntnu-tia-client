import json
from enum import Enum
from typing import List, Optional, Dict

import pydantic


class BaseModel(pydantic.BaseModel):
    @classmethod
    def from_dict(cls, data):
        return cls(**data)

    @classmethod
    def from_json(cls, json_data):
        data = json.loads(json_data)
        return cls.from_dict(data)


class KontaktinformasjonTypeEnum(str, Enum):
    sip_address = 'sip_address'
    email_work = 'email_work'
    email_personal = 'email_personal'


class KeyTypeEnum(str, Enum):
    nin = 'nin'
    ansattnr = 'ansattnr'        # Paga ansattnr
    fslopenr = 'fslopenr'
    sapAnsattnr = 'sapAnsattnr'
    feideid = 'feideid'
    basid = 'basid'
    email = 'email'
    username = 'username'


class NameTypeEnum(str, Enum):
    displayname = 'displayname'
    last_name = 'last_name'
    first_name = 'first_name'
    username = 'username'


class StatusTypeEnum(str, Enum):
    accountActivated = 'accountActivated'
    reservePublish = 'reservePublish'


class RoleTypeEnum(str, Enum):
    affiliated = 'affiliated'
    oppdragstaker = 'oppdragstaker'
    category = 'category'
    arbeidstaker = 'arbeidstaker'
    student = 'student'


class Role(BaseModel):
    actsOn: Dict[str, str]
    qualifier: Dict[str, str]
    _from: Optional[int]  # msEpoch
    to: Optional[int]   # msEpoch


class Person(BaseModel):
    lastUpdated: int    # msEpoch
    dateOfBirth: Optional[int]   # msEpoch
    gender: Optional[str]
    keys: Dict[KeyTypeEnum, str]
    kontaktinformasjon: Optional[Dict[KontaktinformasjonTypeEnum, str]]
    name: Optional[Dict[NameTypeEnum, str]]
    roles: Optional[Dict[RoleTypeEnum, List[Role]]]
    status: Optional[Dict[StatusTypeEnum, str]]
